<html>
	<head>
		<title>Bittorrent Photostream</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<!-- Add jQuery library -->
		<script type="text/javascript" src="fancybox/lib/jquery-1.10.1.min.js"></script>

		<!-- Latest compiled and minified CSS-->
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

		<!-- Optional theme -->
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

		<!-- Latest compiled and minified JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

		<style type="text/css">
			.img-responsive{
				max-width: 90%;
				margin-left: 5%;
			}
		</style>
	</head>

	<body>
		<div class="page-header">
			<h1>Bittorrent Photostream <small>von NAME</small></h1>
		</div>
		<?php
			error_reporting(E_ALL); 
			ini_set( 'display_errors','1');

			$files = scandir("img");

			// check the filenames if they have been changed yet
			function isUnchangedFileName($fileName){
				if(count(explode("btps", $fileName)) > 1){
					return false;
				}else{
					return true;
				}
			}

			function isJPG($fileName){
				if(count(explode(".JPG", $fileName)) > 1){
					return true;
				}else{
					return false;
				}
			}

			// First: Only JPGs are processed
			// Second: Only unchangend files get renamed
			// Files get renamed to btps-MD5Hash-date.JPG
			// So device info won't show up in the file name an there should be no duplicate filenames
			foreach ($files as $value) {
				if(isJPG($value)){
					if(isUnchangedFileName($value)){
						$md5Part = md5($value);
						$deviceParts = explode(" ", $value);
						// there should be left YYYY.MM.DD_XX.JPG
						$dateParts = explode(".", $deviceParts[1]);
						$dateSubParts = explode("_", $dateParts[2]);
						$dateParts[2] = $dateSubParts[0];
						$newName = "img/btps-" . $md5Part . "-" . $dateParts[0] . "." . $dateParts[2] . "." . $dateParts[2] . ".JPG";
						$oldName = "img/" . $value;
						rename($oldName, $newName);
					}
				}
			}

			$files = scandir("img");
			for ($i = 0; $i < count($files); $i++) {
				$parts = explode(".", $files[$i]);
				if($parts[count($parts)-1] == "JPG"){
					echo '<a href="img/' . $files[$i] . '"><img class="img-responsive" src="img/' . $files[$i] . '"></a><br>';
				}
			}
		?>
	</body>
</html>
